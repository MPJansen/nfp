#pragma once
#include "Eigen/Dense"
#include "utils.h"

namespace nfp {

	using coordinate_t = double;

	using point_t = Eigen::Vector<coordinate_t, 2>;
	using point_list_t = Eigen::Matrix2X<coordinate_t>;

	//Folowing: https://eprints.soton.ac.uk/36850/1/CORMSIS-05-05.pdf
	auto minkowski_sum(point_list_t a, point_list_t b);
}