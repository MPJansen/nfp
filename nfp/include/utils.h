#pragma once

#include <Eigen/Dense>

namespace Math {

template<typename T> constexpr T pi_t = static_cast<T>(3.141592653589793238462643383279502884L /* pi */);
constexpr auto pi = pi_t<double>;

template<typename Scalar> constexpr auto sign(Scalar a) { return Scalar((a > Scalar{ 0 }) - (a < Scalar{ 0 })); }

};// namespace Math

namespace Eigen {
template<typename Derived, int rows, int cols, bool innerPanel>
void swap(Eigen::Block<Derived, rows, cols, innerPanel> a, Eigen::Block<Derived, rows, cols, innerPanel> b)
{
    // std::puts("Swap a <=> b");
    a.swap(b);
}
}// namespace Eigen

namespace nfp::helpers {
template<int n, int m, typename Element, typename... Elements> auto colMat(const Element &e, const Elements &... es)
{
    return ((Eigen::Matrix<double, n, m>() << e), ..., es).finished();
}

template<typename Element, typename... Elements> auto hStack(const Element &e, const Elements &... es)
{
    // static_assert((Vectors::rows() == n && ...));
    int rows = e.rows();
    assert(((rows == es.rows()) && ...));

    int cols = (e.cols() + ... + es.cols());

    return ((Eigen::MatrixXd(rows, cols) << e), ..., es).finished();
}

template<typename Element, typename... Elements> auto vStack(const Element &e, const Elements &... es)
{
    // static_assert((Vectors::rows() == n && ...));
    int cols = e.cols();
    assert(((cols == es.cols()) && ...));

    int rows = (e.rows() + ... + es.rows());

    return ((Eigen::MatrixXd(rows, cols) << e), ..., es).finished();
}


}// namespace nfp::helpers