#include "nfp.h"

#include <algorithm>
#include <iostream>
#define GTEST_COUT std::cerr << "[          ] [ INFO ]"


namespace nfp {

namespace internal {

    struct markedVertex
    {
        coordinate_t angle;
        int index;
        bool is_turningPoint;
    };
    auto step2(point_list_t &p)
    {
        auto min_element = std::min_element(p.colwise().begin(), p.colwise().end(), [](const auto &a, const auto &b) {
            auto s = (b - a).cwiseSign();
            return (s(0) == coordinate_t{ 1 }) || (s(0) == coordinate_t{ 0 } && s(1) == coordinate_t{ 1 });
        });

        GTEST_COUT << "p: " << p << std::endl;
        std::rotate(p.colwise().begin(), min_element, p.colwise().end());
        GTEST_COUT << "p: " << p << std::endl;

        using namespace nfp::helpers;

        const auto vectorAngle = [](const auto &prev, const auto &current, const auto &next) {
            using namespace Math;

            point_t u = current - prev;
            point_t v = next - current;
            // auto angle = std::acos((u.dot(v)) / (u.norm() * v.norm())) /* * 180.0 / MathConstants::pi*/;
            auto angle = std::atan2(v.y(), v.x()) - std::atan2(u.y(), u.x());
            return (angle > pi ? angle - pi : angle < -pi ? angle + pi : angle) * 180.0 / pi;
        };
        const auto equalSign = [](const auto &a, const auto &b) { return Math::sign(a) == Math::sign(b); };

        std::vector<markedVertex> alpha;
        alpha.reserve(p.cols());
        {
            alpha.push_back({ vectorAngle(p(Eigen::all, Eigen::last), p(Eigen::all, 0), p(Eigen::all, 1)), 0, false });
            for (int i = 1; i < p.cols() - 1; ++i) {
                auto angle = vectorAngle(p(Eigen::all, i - 1), p(Eigen::all, i), p(Eigen::all, i + 1));
                alpha.push_back({ angle, i, !equalSign(angle, alpha[i - 1].angle) });
            }
            auto angle = vectorAngle(p(Eigen::all, Eigen::last - 1), p(Eigen::all, Eigen::last), p(Eigen::all, 0));
            alpha.push_back({ angle, p.cols(), !equalSign(angle, alpha[p.cols() - 2].angle) });

            alpha.front().is_turningPoint = !equalSign(alpha.front().angle, alpha.back().angle);
        }
    }
}// namespace internal


auto minkowski_sum(point_list_t a_, point_list_t b_)
{
    // Step 1
    auto &a = a_;
    auto b = -b_;
}
}// namespace nfp