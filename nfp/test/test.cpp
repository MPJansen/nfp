#include "gtest/gtest.h"

#include <algorithm>

#include "Eigen/Dense"
#include "nfp.h"

#include "../src/nfp.cpp"

namespace Test_Eigen {
TEST(Swap, ColSwap)
{
    Eigen::MatrixXd m = Eigen::MatrixXd::Identity(4, 10);
    auto columns = m.colwise();

    std::rotate(columns.begin(), columns.begin() + 3, columns.end());
    EXPECT_EQ(m(3, 0), 1.0);
    std::rotate(columns.begin(), columns.end() - 3, columns.end());
    EXPECT_EQ(m, Eigen::MatrixXd::Identity(4, 10));
}
TEST(Stacking, colMat)
{

    auto v = nfp::helpers::colMat<2, 2>(Eigen::Vector2d::Ones(), Eigen::Vector2d::Ones());
    EXPECT_EQ(v.rows(), 2);
    EXPECT_EQ(v.cols(), 2);
}
TEST(Stacking, hStack)
{

    using namespace nfp::helpers;

    Eigen::MatrixXd m = hStack(Eigen::Matrix2d::Identity(), Eigen::Matrix2Xd::Identity(2, 4));

    EXPECT_EQ(m.rows(), 2);
    EXPECT_EQ(m.cols(), 6);
}

TEST(Stacking, vStack)
{

    using namespace nfp::helpers;

    Eigen::MatrixXd m = vStack(Eigen::Matrix2d::Identity(), Eigen::MatrixXd::Identity(4, 2));

    EXPECT_EQ(m.rows(), 6);
    EXPECT_EQ(m.cols(), 2);
}

}// namespace Test_Eigen

namespace Test_Math {
TEST(Math, Pi) { EXPECT_EQ(3.141592653589793238462643383279502884, Math::pi); }
TEST(Math, Sign)
{
    constexpr auto positive = Math::sign(2.0);
    static_assert(positive == 1.0);
    ASSERT_EQ(positive, 1.0);

    constexpr auto negative = Math::sign(-2.0);
    static_assert(negative == -1.0);
    ASSERT_EQ(negative, -1.0);

    constexpr auto nil = Math::sign(0.0);
    static_assert(nil == 0.0);
    ASSERT_EQ(nil, 0.0);
}

}// namespace Test_Math

namespace Test_nfp {
TEST(Minkowski_sum, step2)
{
    using namespace nfp;
    {
        // Box
        point_list_t p(2, 4);
        p << -point_t::UnitX(), point_t::UnitX(), point_t::Ones(), -point_t::Ones();
        nfp::internal::step2(p);
    }
    {
        // House
        point_list_t p(2, 5);
        p << -point_t::UnitX(), point_t::UnitX(), point_t::Ones(), 2 * point_t::UnitY(), -point_t::Ones();
        nfp::internal::step2(p);
    }
    {
        // Star
        point_list_t p(2, 10);
        for (int i = 0; i < 10; ++i) {
            p.col(i) = (2 + std::pow(-1, i))
                       * point_t{ std::sin(i * Math::pi / p.cols() * 2), std::cos(i * Math::pi / p.cols() * 2) };
        }

		nfp::internal::step2(p);
    }
}
}// namespace Test_nfp